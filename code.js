let Timer = React.createClass({
	getInitialState() {
		return {
			minutes: '00',
			seconds: '00',
			isPaused: true
		};
	},
	
	componentDidMount() {
		this.timer = setInterval(this.tick, 1000);
	},
	
	tick() {
		if(!this.state.isPaused) {
			let sec = Number(this.state.seconds);
			
			switch (true) {
				case sec < 9:
					this.setState({ seconds: '0' + ++sec });
					break;
				case sec >= 9 && sec < 59:
					this.setState({ seconds: ++sec });
					break;
				case sec == 59:
					this.setState({ seconds: '00' });
					this.addMinute();
					break;
			}
		}
		
	},
	addMinute() {
		let min = Number(this.state.minutes);
		
		switch (true) {
			case min < 9:
				this.setState({ minutes: '0' + ++min });
				break;
			case min >= 9 && min < 59:
				this.setState({ minutes: ++min });
				break;
		}
	},
	
	componentWillUnmount() {
		clearInterval(this.timer);
	},
	
	reloadTimer() {
		this.setState({
			minutes: '00',
			seconds: '00'
		})
	},
	
	pausePlay(e) {
		this.setState({isPaused: !this.state.isPaused});
		e.target.className = this.state.isPaused ? 'pause' : 'play';
		
	},
	
	render() {
		return (
			<div className="container">
				<input type="button" className="play" onClick={this.pausePlay}/>
				<div className="time-wrapper">
					<p>{this.state.minutes}:{this.state.seconds}</p>
				</div>
				
				<input type="button" className="reload" onClick={this.reloadTimer}/>
			</div>
			
		);
	}
});

ReactDOM.render(
<Timer />,
	document.getElementById('app')
);
